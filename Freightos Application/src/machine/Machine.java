package machine;

public  interface Machine {
    boolean checkIfEmpty(int row, int column);

    void selectItem(int row, int column);

    void refund(int row, int column);

    void resetChoice();

    void fillMachine();

    void fillPrices();

    boolean verify(String input);

    void removeItemFromMachine(int row, int column);

    double verifyMoney(String inputType, String Money);
}
