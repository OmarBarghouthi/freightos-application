package machine;

import keybad.Keybad;

import java.util.Scanner;

public class Driver {

    public static void main(String[] args) {
        SnacksMachine machine = new SnacksMachine();
        machine.fillMachine();
        //machine.display_list();
        machine.fillPrices();
        double paiedMoney;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the way you want to select your money 1:Notes 2:Coins 3:Card");
        int moneyInputType = input.nextInt();
        input.nextLine();
        String inputMoneyValue;
        switch(moneyInputType){
            case 1:
                System.out.println("You have selected Notes please enter one of these notes $20 , $50");
                inputMoneyValue =input.nextLine();
                paiedMoney = machine.verifyMoney("Notes",inputMoneyValue);
                machine.paiedBills= inputMoneyValue;
                machine.paied = paiedMoney;
                break;

            case 2:
                System.out.println("You have selected coins please enter any of these coins $1, C50, C20, C10 ");
                System.out.println("Enter the coins after each other with a ',' between them");
                inputMoneyValue = input.nextLine();
                paiedMoney = machine.verifyMoney("Coins",inputMoneyValue);
                machine.paiedBills= inputMoneyValue;
                machine.paied = paiedMoney;
                break;

            case 3 :
                System.out.println("You have selected a card please insert you card and select the needed value");
                inputMoneyValue = input.nextLine();
                paiedMoney = machine.verifyMoney("Card",inputMoneyValue);
                machine.paied = paiedMoney;
                break;

        }
        System.out.println("Enter the snack code like the following format (A0 for the element on top left and E4 for the one on bottom left) : ");

        String snackCode = input.nextLine();
        if(machine.verify(snackCode)){
            System.out.print("Your snack is available");
            int[] pos = Keybad.NormalizaInput(snackCode);
            machine.selectItem(pos[0],pos[1]);
        }
        else{
            System.out.println("The snack is not available");
        }
    }
}
