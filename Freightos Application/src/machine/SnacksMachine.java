package machine;

//import Exceptions.IncorrectMoneyException;

import keybad.Keybad;
import money.Coins;
import money.Notes;
import snacks.Snacks;

import java.io.File;
import java.util.*;

public class SnacksMachine implements Machine {
    //A 3D machine that will hold the object
    Snacks[][][] machine;
    //A 2D array to represent the prices of each slot
    double[][] priceArray;
    //This is the money inventory in the machine
    double[][] moneyInventory = {{50.0, 20.0, 1.0, 0.50, 0.20, 0.10}, {2, 0, 25, 25, 25, 25}};
    //This is a keybad that will normalize the user input into data in order to check the location
    Keybad keybad;
    //This is the bills that the user entered
    String paiedBills;
    //This is the total value of the bills that the user entered
    double paied;
    //This is to check if an element was sold or not
    boolean sold;

    /**
     * initialize vending machine with default snacks
     */
    public SnacksMachine() {
        this.machine = new Snacks[5][5][10];
        this.priceArray = new double[5][5];
        this.keybad = new Keybad();
    }

    /**
     * This function will check if that object is empty or not
     */
    @Override
    public boolean checkIfEmpty(int row, int column) {
        for (int i = 0; i < machine[row][column].length; i++) {
            if (machine[row][column][i] == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * This function is used to choose the selected snack and confirm if the user didn't change his mind
     */
    @Override
    public void selectItem(int row, int column) {
        System.out.println(" PLEASE CONFIRM YOUR SELECTION OF " + machine[row][column][0].name);
        System.out.println("Type yes or no to confirm your selection");
        Scanner Scan = new Scanner(System.in);
        String confirmationcode = Scan.nextLine().toLowerCase().trim();
        if (confirmationcode.equals("yes")) {
            System.out.println("Checking the money");
            if (checkMoney(row, column)) {
                sold = true;
                refund(row, column);
                removeItemFromMachine(row, column);
            } else if (paiedBills == null) {
                System.out.print("You used a card");
                if (paied >= priceArray[row][column]) {
                    System.out.println("Good card");
                    removeItemFromMachine(row, column);
                } else {
                    System.out.println("Not enought money");
                    System.out.println("paied" + paied);
                    System.out.println(priceArray[row][column]);
                }
            } else {
                System.out.println("Not enought money");
            }
        } else {
            System.out.println("Do you want to continue?");
            System.out.println("YES or NO");
            confirmationcode = Scan.nextLine().toLowerCase().trim();
            if (confirmationcode.equals("yes")) {
                resetChoice();
            } else {
                System.out.println("Thanks for using this machine");
                if (paiedBills.equals(null)) {
                    System.exit(1);
                } else
                    refund(row, column);
            }
        }
    }

    /**
     * This function is used to check if the money is enough or not
     */
    boolean checkMoney(int row, int column) {
        if (priceArray[row][column] > paied)
            return false;
        return true;
    }

    /**
     * This is the refund function if no item is sold it will refund the original input
     */
    @Override
    public void refund(int row, int column) {
        double[] prices = new double[6];
        double[] toBeReturned = new double[6];
        int i = 0;
        for (Notes note : Notes.values()) {
            prices[i] = note.getValue();
            i++;
        }
        for (Coins coin : Coins.values()) {
            prices[i] = coin.getValue();
            i++;
        }
        if (!sold) {
            System.out.println("No item was sold return money");
        } else if (paiedBills == null) {
            System.out.println(" A card was used no refund");
        } else {
            double returnedPrice = paied - priceArray[row][column];
            System.out.println(returnedPrice);
            for (int j = 0; j < prices.length; j++) {
                if (returnedPrice >= prices[j]) {
                    if (moneyInventory[1][j] != 0) {
                        returnedPrice -= prices[j];
                        toBeReturned[j]++;
                        moneyInventory[1][j]--;
                        --j;
                    }
                }
            }
            if (returnedPrice != 0) {
                System.out.println("Not enough money in the inventory");
            } else {
                for (int j = 0; j < toBeReturned.length; j++) {
                    if (toBeReturned[j] != 0) {
                        System.out.println("Bill " + moneyInventory[0][j] + " # of times" + toBeReturned[j]);
                    }
                }
            }
        }

    }

    /**
     * This function will be used if the user decides to reset his choice
     */
    @Override
    public void resetChoice() {
        Scanner confirmationScan = new Scanner(System.in);
        System.out.println("Enter the snack code like the following format (A0 for the element on top left and E4 for the one on bottom left) :");
        String newCode = confirmationScan.nextLine();
        int[] pos = Keybad.NormalizaInput(newCode);
        if (verify(newCode)) {
            System.out.print("Your snack is available");
            selectItem(pos[0], pos[1]);
        } else {
            System.out.println("Your snack is not available");
        }
    }

    /**
     * This function will fill the machine with the snacks list from a file
     */
    @Override
    public void fillMachine() {
        try {
            File snacks = new File("src/SnacksFile");
            Scanner snacksScanner = new Scanner(snacks);
            for (int i = 0; i < machine.length; i++) {
                for (int j = 0; j < machine[i].length; j++) {
                    if (snacksScanner.hasNext()) {
                        String snackName = snacksScanner.nextLine();
                        for (int k = 0; k < machine[i][j].length; k++) {
                            machine[i][j][k] = new Snacks(snackName);
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * This function will fill the 2d array of the price of the list from a file
     */
    @Override
    public void fillPrices() {
        try {
            File prices = new File("src/Prices");
            Scanner pricesScanner = new Scanner(prices);
            for (int i = 0; i < priceArray.length; i++) {
                for (int j = 0; j < priceArray[i].length; j++) {
                    priceArray[i][j] = pricesScanner.nextDouble();
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * This function will call both function in keybad in order to use the results here
     */
    @Override
    public boolean verify(String input) {
        int[] pos = Keybad.NormalizaInput(input);
        if (Keybad.verifyInput(pos)) {
            if (checkIfEmpty(pos[0], pos[1])) {
                System.out.println("This snack is not empty");
                return true;
            } else {
                System.out.println("This snack is not available");
                return false;
            }
        } else
            return false;
    }

    /**
     * This function will remove one of the items in the machine if it get sold
     */
    @Override
    public void removeItemFromMachine(int row, int column) {
        for (int k = machine[row][column].length - 1; k >= 0; k--) {
            if (machine[row][column][k] != null) {
                machine[row][column][k] = null;
                break;
            }
        }
    }

    /**
     * This function will verify if the money is Correct or not
     */
    @Override
    public double verifyMoney(String inputType, String Money) {
        double total = 0.0;
        switch (inputType) {
            case "Notes":
                String[] notes = Money.split(",");
                total = 0.0;
                Notes note;
                for (int i = 0; i < notes.length; i++) {
                    note = Notes.valueOf(notes[i].trim());
                    if (note instanceof Notes) {
                        total += ((Notes) note).getValue();
                    }
                }
                break;

            case "Coins":
                String[] coins = Money.split(",");
                total = 0.0;
                Coins coin;
                for (int i = 0; i < coins.length; i++) {
                    if (Coins.valueOf(coins[i].trim()) == null) {
                        coin = Coins.valueOf(coins[i].trim());
                        if (coin instanceof Coins) {
                            total += ((Coins) coin).getValue();
                        }
                    }
                }
                break;
            case "Card":
                System.out.println("Card is good");
                total = Double.parseDouble(Money);
        }
        return total;
    }
}
