package snacks;

public class Snacks {
    public String name;

    public Snacks(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
