package money;

public enum Coins {
    $1(1.0), C50(0.5), C20(0.2), C10(0.1);
    private double value;

    Coins(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
