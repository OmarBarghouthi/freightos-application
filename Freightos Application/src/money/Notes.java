package money;

public enum Notes {
    $50(50.0), $20(20.0);
    private double value;

    Notes(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
