package keybad;

public class Keybad {

    public Keybad() {

    }
    /**
     * This function is used to normalize the user input inorder to get the position of the element
     */
    public static int[] NormalizaInput(String input) {
        int[] keyIndex = new int[2];
        input = input.toLowerCase();
        keyIndex[0] = input.charAt(0) - 97;
        keyIndex[1] = Integer.parseInt(String.valueOf(input.charAt(1)));
        return keyIndex;
    }
    /**
     * This function will verify if the input is valid or not
     */
    public static boolean verifyInput(int[] keyIndex) {
        if (keyIndex[0] < 5 && keyIndex[1] < 5) {
            return true;
        }
        return false;
    }
}
